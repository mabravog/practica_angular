angular.module("FinalApp",["ngRoute","ngResource","lumx"])
.config(function($routeProvider){
	$routeProvider
	.when("/",{
		templateUrl: 'templates/home.html',
    	controller: 'MainController'
	})
	.when("/post/:id",{
		templateUrl: 'templates/post.html',
    	controller: 'PostController'
	})
	.when("/posts/new",{
		templateUrl: 'templates/post_form.html',
    	controller: 'NewPostController'
	})
	.when("/post/edit/:id",{
		templateUrl: 'templates/post_form.html',
    	controller: 'PostController'
	})
});