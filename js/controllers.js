angular.module("FinalApp")
.controller("MainController",function($scope,$resource,PostResource,UserResource){
	$scope.posts = PostResource.query();
	$scope.users = UserResource.query();

	$scope.removePost = function(post){
		PostResource.delete({id: post.id}, function(data){
			if(data.$resolved){
				$scope.posts = $scope.posts.filter(function(element){
					return element.id !== post.id;
				});
			} else {
				console.log("El post no pudo ser eliminado.");
			};
		});
	}
})
.controller('PostController', function($scope,$routeParams,$location,PostResource,LxNotificationService){
	$scope.title = "Edit post"
	$scope.post = PostResource.get({id: $routeParams.id});

	$scope.savePost = function(){
		PostResource.update({id: $scope.post.id},{data: $scope.post}, function(data){
			console.log(data);
			guardado = data.$resolved;
			if(guardado) {
		       	LxNotificationService.success('Updated');
		       	$location.path('/');
		    } else {
		    	LxNotificationService.error('Not updated');
		    }
		});
	}
})
.controller('NewPostController', function($scope,$resource,$location,LxNotificationService,PostResource){
	$scope.post = {};
	$scope.title = "New post"
	var guardado = false;

	$scope.savePost = function(){
		PostResource.save({data: $scope.post}, function(data){
			console.log(data);
			guardado = data.$resolved;
			if(guardado) {
		       	LxNotificationService.success('Saved');
		       	$location.path('/');
		    } else {
		    	LxNotificationService.error('Not saved');
		    }
		});
	}
});